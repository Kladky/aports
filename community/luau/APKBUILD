# Contributor: Rob Blanckaert <basicer@gmail.com>
# Maintainer: Rob Blanckaert <basicer@gmail.com>
pkgname=luau
pkgver=0.573
pkgrel=0
pkgdesc="A fast, small, safe, gradually typed embeddable scripting language derived from Lua"
url="https://github.com/roblox/luau"
arch="all"
license="MIT"
makedepends="cmake linux-headers samurai"
source="
$pkgname-$pkgver.tar.gz::https://github.com/Roblox/luau/archive/refs/tags/$pkgver.tar.gz
"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	"$builddir"/build/Luau.UnitTest && "$builddir"/build/Luau.Conformance
}

package() {
	install -Dm755 build/luau "$pkgdir"/usr/bin/luau
	install -Dm755 build/luau-analyze "$pkgdir"/usr/bin/luau-analyze
}

sha512sums="
c3584ecf0575b1c9f980c00c088e41ec33165f5869aaa1ab0bff0866d0733861b7d6a22c197720bdf4d2fe3e55cd6626005205876601c6cb82bd14ea59012098  luau-0.573.tar.gz
"

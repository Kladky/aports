# Contributor: Mark Riedesel <mark@klowner.com>
# Contributor: Damian Kurek <starfire24680@gmail.com>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=openimageio
pkgver=2.4.11.0
pkgrel=1
pkgdesc="Image I/O library supporting a multitude of image formats"
options="!check" # more than 10% of all tests fail
url="https://sites.google.com/site/openimageio/"
# s390x has missing dependency ptex-dev
arch="all !s390x"
license="BSD-3-Clause"
makedepends="cmake
	boost-dev
	bzip2-dev
	ffmpeg-dev
	fmt-dev
	freetype-dev
	giflib-dev
	hdf5-dev
	libheif-dev
	libtbb-dev
	libraw-dev
	libwebp-dev
	mesa-dev
	opencolorio-dev
	openexr-dev
	openjpeg-dev
	ptex-dev
	ptex-static
	python3-dev
	py3-pybind11-dev
	qt5-qtbase-dev
	robin-map
	samurai
	tiff-dev
	"
subpackages="py3-$pkgname:_python $pkgname-dev $pkgname-doc $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenImageIO/oiio/archive/v$pkgver.tar.gz
	CVE-2023-36183.patch
	"
builddir="$srcdir/oiio-$pkgver"

# secfixes:
#   2.4.11.0-r1:
#     - CVE-2023-36183

build() {
	local _py_version=$(python3 --version | cut -c 8-11)
	local _iv="ON"

	case "$CARCH" in
		aarch64|armv7|armhf)
			_iv="OFF";;
	esac

	# fails to build with fortify source enabled
	export CXXFLAGS="$CXXFLAGS -U_FORTIFY_SOURCE"

	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_SKIP_RPATH=ON \
		-DBUILD_TESTING=OFF \
		-DSTOP_ON_WARNING=OFF \
		-DENABLE_iv=$_iv \
		-DINSTALL_FONTS=OFF
	cmake --build build
}

check() {
	cd build
	ctest -E broken
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

tools() {
	pkgdesc="Tools for manipulating a multitude of image formats"

	amove usr/bin
}

_python() {
	local pyver="${subpkgname:2:1}"
	pkgdesc="Python $pyver bindings for OpenImageIO image I/O library"
	depends="python$pyver"

	amove usr/lib/python*
}

sha512sums="
164acbda8ecd357b6d375386dbed2aded404bbaec5b50ffff1adbcc31dd804670d3b120f053c951e3e654209f3f70c3987928ed45802a57f3b847b6905a29d9e  openimageio-2.4.11.0.tar.gz
c6b61f710c8fbe593b702dc1b2d68d066d6efe1eb4f2d76ef81601d73711541b7eec0f55830dd611eb6fc544e0b3750ba5bcaf7da1d74e7a4179b8089b2f2b0d  CVE-2023-36183.patch
"

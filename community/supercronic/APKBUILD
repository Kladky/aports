# Contributor: Thomas Kienlen <kommander@laposte.net>
# Maintainer: Thomas Kienlen <kommander@laposte.net>
pkgname=supercronic
pkgver=0.2.24
pkgrel=1
pkgdesc="Cron for containers"
url="https://github.com/aptible/supercronic"
arch="all"
license="MIT"
makedepends="go"
checkdepends="python3"
source="supercronic-$pkgver.tar.gz::https://github.com/aptible/supercronic/archive/refs/tags/v$pkgver.tar.gz
	python3.patch
	"
options="chmod-clean"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -trimpath
}

check() {
	# make unit (without -race option, -buildmode=pie not supported when -race is enabled)
	go test -v ./...
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
0657aa0d9aa2381c32427edb5cc069d4ff7a43e6960a87dd181af9039037501ae6e2af9cd6c8ab0e43988caabe4552edd14a095410fa435a448f9011c32ba3c3  supercronic-0.2.24.tar.gz
fd88eccd0a18d65af57292e430a1c69b2de1d5c845522e1e1b8b217ab11b8d974543da747debbd6cd2c03d0929dcc04c028a0dc95fc27433331a0dec61d1dfae  python3.patch
"

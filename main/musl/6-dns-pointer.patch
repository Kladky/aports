Patch-Source: https://git.musl-libc.org/cgit/musl/commit/?id=83b858f83b658bd34eca5d8ad4d145f673ae7e5e
--
From 83b858f83b658bd34eca5d8ad4d145f673ae7e5e Mon Sep 17 00:00:00 2001
From: Rich Felker <dalias@aerifal.cx>
Date: Mon, 17 Jul 2023 18:03:38 -0400
Subject: fix rejection of dns responses with pointers past 512 byte offset

the __dns_parse code used by the stub resolver traditionally included
code to reject label pointers to offsets past a 512 byte limit,
despite never processing the label contents, only stepping over them.
when commit 51d4669fb97782f6a66606da852b5afd49a08001 added support for
tcp fallback, this limit was overlooked, and as a result, it was at
least theoretically possible for some valid large answers to be
rejected on account of these offsets.

since the limit was never serving any useful purpose, just remove it.
---
 src/network/dns_parse.c | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/src/network/dns_parse.c b/src/network/dns_parse.c
index 7f83e791..ea1ec126 100644
--- a/src/network/dns_parse.c
+++ b/src/network/dns_parse.c
@@ -15,13 +15,13 @@ int __dns_parse(const unsigned char *r, int rlen, int (*callback)(void *, int, c
 	if (qdcount+ancount > 64) return -1;
 	while (qdcount--) {
 		while (p-r < rlen && *p-1U < 127) p++;
-		if (p>r+rlen-6 || *p>193 || (*p==193 && p[1]>254))
+		if (p>r+rlen-6)
 			return -1;
 		p += 5 + !!*p;
 	}
 	while (ancount--) {
 		while (p-r < rlen && *p-1U < 127) p++;
-		if (p>r+rlen-12 || *p>193 || (*p==193 && p[1]>254))
+		if (p>r+rlen-12)
 			return -1;
 		p += 1 + !!*p;
 		len = p[8]*256 + p[9];
-- 
cgit v1.2.1


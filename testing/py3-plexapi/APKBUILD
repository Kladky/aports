# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-plexapi
_pkgname=python-plexapi
pkgver=4.13.4
pkgrel=0
pkgdesc="Python bindings for the Plex API"
url="https://github.com/pkkid/python-plexapi"
arch="noarch"
license="BSD-3-Clause"
# tests requires an instance of plex running
# net for sphinx
options="net !check"
depends="
	python3
	py3-requests
	"
makedepends="
	py3-gpep517
	py3-recommonmark
	py3-setuptools
	py3-sphinx_rtd_theme
	py3-wheel
	"
subpackages="$pkgname-doc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/pkkid/python-plexapi/archive/$pkgver.tar.gz
	0001-sphinx-language.patch
	"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
	sphinx-build -W -b man docs man
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
	install -Dm644 man/pythonplexapi.1 -t "$pkgdir"/usr/share/man/man1
}

sha512sums="
bc48da7c3005ffcae3cce4399031808165e6acad2883a64f45cd2b35f2863d8083e886d84552ece4339cd5da20ac50db55115255c68642a69826585a45f53553  py3-plexapi-4.13.4.tar.gz
015c75b2a09ef6d82aedd18302e7bc4249f25aa12b1dbc3a67e4259330c2a50db6467504f29b911975dc1849fcccfe249145593abf5470c6b78e4181b5f9f7c5  0001-sphinx-language.patch
"
